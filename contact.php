<?php
require_once "recaptcha.php";
if (isset($_POST['email'])) {

    $email_to = "postmaster@2t-labs.ru";
    $email_subject = "Контактная форма 2t-labs";

    $secret = "6Lcam54UAAAAAIsCxV7GBEuJx9nwQD8EDW_Cwab_";
    $response = null;
    $reCaptcha = new ReCaptcha($secret);

    function died($error, $captcha_error) {
        if (strlen($error) > 0) {
            echo "Данные формы содержат ошибки<br /><br />";
            echo $error . "<br />";
            echo "Пожалуйста, введите валидные данные и повторите отправку<br /><br />";
        }
        echo $captcha_error;
        die();
    }

    // validation expected data exists
    if (!isset($_POST['name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['phone']) ||
        !isset($_POST['message'])) {
        http_response_code(400);
        died("", "");
    }

    $name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
    $telephone = $_POST['phone']; // not required
    $message = $_POST['message']; // required

    $error_message = "";
    $error_captcha = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

    if (!preg_match($email_exp, $email_from)) {
        $error_message .= 'Введенный Email невалидный<br />';
    }

    $string_exp = "/^[A-Za-z .'-]+$/";


    if (strlen($message) < 2) {
        $error_message .= 'Сообщение невалидно<br />';
    }

    // if submitted check response
    if ($_POST["g-recaptcha-response"]) {
        $response = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g-recaptcha-response"]
        );
    }

    if (!$response || !$response->success) {
        http_response_code(400);
        $error_captcha .= 'Подтвердите, что вы не робот';
    }

    if (strlen($error_message) > 0 || strlen($error_captcha) > 0) {
        http_response_code(400);
        died($error_message, $error_captcha);
    }

    $email_message = "Детали формы ниже.\n\n";

    function clean_string($string)
    {
        $bad = array("content-type", "bcc:", "to:", "cc:", "href");
        return str_replace($bad, "", $string);
    }

    $email_message .= "Имя: " . clean_string($name) . "\n";
    $email_message .= "Email: " . clean_string($email_from) . "\n";
    $email_message .= "Телефон: " . clean_string($phone) . "\n";
    $email_message .= "Сообщение: " . clean_string($message) . "\n";


    $headers = 'From: ' . $email_from . "\r\n" .
        'Reply-To: ' . $email_from . "\r\n" .
        'X-Mailer: PHP/' . phpversion() . "\r\n" .
        'Content-Type: text/plain; charset=utf-8' . "\r\n" .
        'Content-Transfer-Encoding: 8bit';

    @mail($email_to, $email_subject, $email_message, $headers);
    ?>
    Спасибо, мы скоро с Вами свяжемся

    <?php

}
?>