$(function() {

  const options = {
    nextButton: true,
    prevButton: true,
    pagination: true,
    animateStartingFrameIn: true,
    autoPlay: true,
    autoPlayDelay: 3000,
    preloader: true,
    preloadTheseFrames: [1],
  };

  $(window).scroll(() => {
    if ($(this).scrollTop() > 1200) {
      $('#top-bt:hidden').css('visibility', 'visible');
      $('#top-bt:hidden').fadeIn('550');
    }
    else {
      $('#top-bt:visible').fadeOut('550');
    }
    const menuHeight = $('.main-menu').outerHeight(),
      orderSectionTop = $('#order').offset().top - menuHeight,
      paymentSectionTop = $('#payment').offset().top - menuHeight,
      contactSectionTop = $('#contact').offset().top - menuHeight;

    if ($(this).scrollTop() > contactSectionTop - 1) {
      markActiveMenuItem($('.main-menu li:nth-child(4) a'));
    } else if ($(this).scrollTop() > paymentSectionTop - 1) {
      markActiveMenuItem($('.main-menu li:nth-child(3) a'));
    } else if ($(this).scrollTop() > orderSectionTop - 1) {
      markActiveMenuItem($('.main-menu li:nth-child(2) a'));
    } else {
      markActiveMenuItem($('.main-menu li:nth-child(1) a'));
    }
  });

  $('.main-menu li a, .button-top a, .up a').click(function (e) {
    const href = $(this).attr('href');
    $('.navbar-collapse.show').removeClass('show').addClass('collapse');
    const scrollTo = $(href).offset().top - $('.main-menu').outerHeight();
    $('html, body').animate({scrollTop: scrollTo}, 500);
  });

  $('.question').click(function () {
    $(this).next('.answer').toggleClass('d-none');
  });

  $(window).on('resize', function () {
    $('body').css('padding-top', $('.main-menu').outerHeight() + 'px');
  });

  $('body').css('padding-top', $('.main-menu').outerHeight() + 'px');


  let request;

  $("#contactForm").submit(function(e){

    e.preventDefault();

    if (request) {
      request.abort();
    }

    const $form = $(this),
      $inputs = $form.find("input, select, button, textarea"),
      serializedData = $form.serialize();

    $inputs.prop("disabled", true);

    request = $.ajax({
      url: "/contact.php",
      type: "post",
      data: serializedData
    });

    request.done(function (response, textStatus, jqXHR){
      showTooltip(textStatus, response);
      $inputs.val('');
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
      showTooltip(textStatus, jqXHR.responseText);
    });

    request.always(function () {
      $inputs.prop("disabled", false);
    });

  });

});

function markActiveMenuItem(item) {
  $('.main-menu li a').removeClass('active');
  item.addClass('active');
}

function showTooltip(status, response) {
  $('.tooltip').html(response);
  $('.tooltip').addClass(status).show();
  setTimeout(() => {
    $('.tooltip').hide();
    setTimeout(() => {
      $('.tooltip').removeClass(status);
    }, 500);
  }, 5000);
}